# Org Mode Spacemacs Visual Line Configuration Layer

My custom org mode visual line configuration layer using [Visual Fill Column](https://codeberg.org/joostkremers/visual-fill-column) package

This configuration layer make spacemacs display org file like this.
![preview](https://gitlab.com/hi.anto/dotspacemacsdotd/-/raw/main/preview/org-mode-visual-fill-layer-preview.png)
