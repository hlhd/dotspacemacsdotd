* Org Roam Spacemacs Configuration Layer
My custom spacemacs configuration layer for using [[https://github.com/org-roam/org-roam][org-roam]], this layer is based on [[https://github.com/joshmedeski/org-roam-spacemacs-layer][org-roam-spacemacs-layer]] by [[https://github.com/joshmedeski/org-roam-spacemacs-layer][Josh Medeski]]
