(setq org-roam-v2-ack t
      org-roam-directory "~/roam-notes"
      org-roam-completion-everywhere t
      org-roam-capture-templates
      '(("d" "default" plain
         "%?"
         :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n#+filetags: %^{Filetags}")
         :unnarrowed t)
        ("s" "sql" plain (file "~/roam-notes/templates/sql-note.org")
         :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n#+filetags: :sql:")
         :unnarrowed t)
        ("b" "book or article notes" plain (file "~/roam-notes/templates/book-or-article.org")
         :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
         :unnarrowed t)
        ("p" "project" plain (file "~/roam-notes/templates/project-note.org")
         :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n#+filetags: Project")
         :unnarrowed t)))
